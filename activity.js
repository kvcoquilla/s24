db.scientist.insertMany([
{
    firstName:"Nikola",
    lastName:"Tesla",
    age: 64,
    department: "Communication"
},
{
    firstName:"Marie",
    lastName:"Curie",
    age: 28,
    department: "Media and Arts"
},
{
    firstName:"Charles",
    lastName:"Darwin",
    age: 51,
    department: "Human Resources"
},
{
    firstName:"Rene",
    lastName:"Descartes",
    age: 50,
    department: "Media and Arts"
},
{
    firstName:"Albert",
    lastName:"Einstein",
    age: 51,
    department: "Human Resources"
},
{
    firstName:"Michael",
    lastName:"Faraday",
    age: 30,
    department: "Communication"
},
]);

// 2.
db.scientist.find(
{$or: 
    [
        {firstName: {$regex: 's',$options: '$i'}},
        {lastName: {$regex: 'd',$options: '$i'}}
    ]
},
{firstName:1,lastName:1,_id:0}
);

// 3.
db.scientist.find({$and:
    [
        {department:"Human Resources"},
        {age: {$gte: 50}}
    ]
});

// 4.

db.scientist.find(
{$and:
    [
        {firstName: {$regex: 'E',$options: '$i'}},
        {age: {$lte: 30}}
    ]
});