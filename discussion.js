db.inventory.insertMany([
	{
		name: "Captain America Shield",
		price: 50000,
		qty:17,
		company: "Hydra and Co"
	},
	{
		name: "Mjolnir",
		price: 75000,
		qty:24,
		company: "Asgard Production"
	},
	{
		name: "Iron Man Suit",
		price: 25400,
		qty:25,
		company: "Stark Industries"
	},
	{
		name: "Eye of Agamotto",
		price: 28000,
		qty:51,
		company: "Sanctum Company"
	},
	{
		name: "Iron Spider Suit",
		price: 30000,
		qty:24,
		company: "Stark Industries"
	}
]);

db.inventory.find({$and: 

	[
		{$regex: 'A'},
		{price: {$lte : 30000}}

	]

},
{name: 1, price:1, _id:0}
)